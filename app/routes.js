const { names, exchangeRates } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({ data: {} })
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        })
    });
    //PERSON ENDPOINT
    app.post('/person', (req, res) => {
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                'error': 'Bad Request : missing required parameter NAME'
            })
        }
        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request : NAME has to be string'
            })
        }
        if (!req.body.hasOwnProperty('age')) {
            return res.status(400).send({
                'error': 'Bad Request: missing required parameter AGE'
            })
        }
        if (typeof req.body.age !== 'number') {
            return res.status(400).send({
                'error': 'Bad Request: AGE has to be a number'
            })
        }
        if (!req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                'error': 'Bad Request : missing required parameter ALIAS'
            })
        }
    })
}
//CURRENCY ENDPOINT

module.exports = (app) => {
    app.get("/currency", (req, res) => {
        return res.send({
            currencies: exchangeRates,
        });
    });

    app.post('/currency', (req, res) => {
        if (!req.body.hasOwnProperty('name')) {
            return res.status(400).send({
                'error': 'Bad Request : missing required parameter NAME'
            })
        }
        if (typeof req.body.name !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request : NAME has to be string'
            })
        }
        if (req.body.name === "") {
            return res.status(400).send({
                'error': 'Bad Request : NAME cannot be empty string'
            })
        }
        if (!req.body.hasOwnProperty('ex')) {
            return res.status(400).send({
                'error': 'Bad Request : EX is missing from parameters'
            })
        }
        if (typeof req.body.ex !== 'object') {
            return res.status(400).send({
                'error': 'Bad Request: EX has to be an object'
            })
        }
        if (Object.keys(req.body.ex).length === 0) {
            return res.status(400).send({
                'error': 'Bad Request: EX cant be an empty object'
            })
        }
        if (!req.body.hasOwnProperty('alias')) {
            return res.status(400).send({
                'error': 'Bad Request : missing required parameter ALIAS'
            })
        }
        if (typeof req.body.alias !== 'string') {
            return res.status(400).send({
                'error': 'Bad Request : ALIAS has to be a string'
            })
        }
        if (req.body.alias === "") {
            return res.status(400).send({
                'error': 'Bad Request : ALIAS cannot be empty string'
            })
        }
        if (req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')) {
            Object.values(exchangeRates).forEach((val) => {
                if (val.alias === req.body.alias) {
                    return res.status(400).send({
                        'error': 'Bad Request: all fields complete but there is duplicate ALIAS'
                    });
                }
            })
                return res.status(200).send({
                    'success': 'Fields are complete and there is no duplicate alias'
                });
        } 
        // else return res.status(400).send({
        //     'error': 'Incomplete fields were provided'
        // });

    })
}