const chai = require('chai')
const expect = chai.expect
// or const { expect } =  require('chai')
const http = require('chai-http')

chai.use(http)

describe('API Test Suite', () => {
    it('Test API Get people is running', () => {
        chai.request('http://localhost:5002').get('/people')
            .end((err, res) => {
                expect(res).to.not.equal(undefined)
            })
    })
    it('Test API get people returns 200', () => {
        chai.request('http://localhost:5002').get('/people')
            .end((err, res) => {
                expect(res.status).to.equal(200)
            })
    })

    it('Test API post person returns 400 if no NAME property', (done) => {
        chai.request('http://localhost:5002').post('/person')
            .type('json')
            .send({
                // name: "Ackerman",
                alias: "Mikasa",
                age: 27
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Test that POST endpoint is running', () => {
        chai.request('http://localhost:5002').post('/person')
            .send({
                alias: "Colossal Titan",
                name: "Armin",
                age: 28
            })
            .end((err, res) => {
                expect(res).to.not.equal(undefined)
            })
    })
    it('Test that POST endpoint encounters an error if no alias', (done) => {
        chai.request('http://localhost:5002').post('/person')
            .type('json')
            .send({
                name: "Ackerman",
                // alias: "Mikasa"
                age: 27
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Test that POST endpoint encounters an error if no age', (done) => {
        chai.request('http://localhost:5002').post('/person')
            .type('json')
            .send({
                name: "Ackerman",
                alias: "Mikasa"
                // age: 27
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
})

//Currency API

describe('Currency API Test Suite', () => {
    it('Check if post /currency is running', () => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': 'United States Dollar',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res).to.not.equal(undefined)
            })
    })
    it('Check if post /currency returns status 400 if name is missing', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                // 'name': 'United States Dollar',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if name is not a string', () => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': 12,
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            })
    })
    it('Check if post /currency returns status 400 if name is empty string', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': '',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    //problem

    it('Check if post /currency returns status 400 if ex is missing', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': 'United States Dollar'
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if ex is not an object', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': 'United States Dollar',
                'ex': 12
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if ex is an empty object', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'dollars',
                'name': 'United States Dollar',
                'ex': {}
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    //not yet done editing here
    it('Check if post /currency returns status 400 if alias is missing', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'name': 'United States Dollar',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if alias is not a string', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 12,
                'name': 'United States Dollar',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if alias is empty string', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': '',
                'name': 'United States Dollar',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    it('Check if post /currency returns status 400 if fields are complete but there is duplicate alias', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'name': 'United States Dollar',
                'alias': 'dollars',
                'ex': {
                    'peso': 50.73,
                    'won': 1187.24,
                    'yen': 108.63,
                    'yuan': 7.03
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
                done();
            })
    })
    //
    it('Check if post /currency returns status 200 if fields are complete and no duplicates', (done) => {
        chai.request('http://localhost:5002').post('/currency')
            .send({
                'alias': 'euro',
                'name': 'European Euro',
                'ex': {
                    'peso': 1,
                    'won': 2,
                    'yen': 3,
                    'yuan': 4
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(200);
                done();
            })
    })
})